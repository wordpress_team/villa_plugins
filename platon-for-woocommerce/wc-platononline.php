<?php
/*
Plugin Name: WooCommerce PlatonOnline Gateway
*/
if (!defined('ABSPATH')) exit;

add_action('plugins_loaded', 'platononline_init', 0);

function platononline_init()
{
    if (!class_exists('WC_Payment_Gateway')) return;

    class WC_Gateway_Platononline extends WC_Payment_Gateway
    {

        public function __construct()
        {

            global $woocommerce;

            $plugin_dir = plugin_dir_url(__FILE__);
            $this->userr = wp_get_current_user();
            $this->id = 'platononline';
            $this->icon = apply_filters('woocommerce_platononline_icon', '' . $plugin_dir . 'platononline.png');
            $this->has_fields = false;
            $this->method_title = __('Platon 1.5', 'woocommerce');
            $this->method_description = __('Platon 1.5', 'woocommerce');
            $this->init_form_fields();
            $this->init_settings();
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->password = $this->get_option('password');
            $this->secret = $this->get_option('secret');
            $this->url = $this->get_option('url');
            $this->language = $this->get_option('language');
            $this->paymenttime = $this->get_option('paymenttime');
            $this->payment_method = $this->get_option('payment_method');
            // Actions
            add_action('woocommerce_receipt_platononline', array($this, 'receipt_page'));
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));

            // Payment listener/API hook
            add_action('woocommerce_api_wc_gateway_platononline', array($this, 'check_ipn_response'));

            if (!$this->is_valid_for_use()) {
                $this->enabled = false;
            }
        }

        public function admin_options()
        {
            ?>
            <h3><?php _e('Platon Online 1.5', 'woocommerce'); ?></h3>

            <?php if ($this->is_valid_for_use()) : ?>

            <table class="form-table">
                <?php
                $this->generate_settings_html();
                ?>
                <p>
                    <strong><?php _e('Сообщите в тех. поддержку platon.ua что ваш Callback Url: ') ?></strong><?php echo str_replace('https:', 'http:', add_query_arg('wc-api', 'WC_Gateway_Platononline', home_url('/'))); ?>
                </p>
            </table>

        <?php else : ?>
            <div class="inline error"><p>
                    <strong><?php _e('Шлюз отключен', 'woocommerce'); ?></strong>: <?php _e('Platon Online не поддерживает валюты Вашего магазина.', 'woocommerce'); ?>
                </p></div>
            <?php
        endif;
        }

        public function init_form_fields()
        {

            $this->form_fields = array(
                'enabled' => array(
                    'title' => __('Включить/Отключить', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('Включить', 'woocommerce'),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('Заголовок', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Заголовок, который отображается на странице оформления заказа', 'woocommerce'),
                    'default' => 'Платон Онлайн',
                    'desc_tip' => true,
                ),
                'description' => array(
                    'title' => __('Описание', 'woocommerce'),
                    'type' => 'textarea',
                    'description' => __('Описание, которое отображается в процессе выбора формы оплаты', 'woocommerce'),
                    'default' => __('Оплатить через электронную платежную систему Platon (<a href="http://platon.ua/">platon.ua</a>)', 'woocommerce'),
                ),
                'url' => array(
                    'title' => __('Url', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Url выданный platon.ua для отправки платежного POST запроса', 'woocommerce'),
                ),
                'secret' => array(
                    'title' => __('Секретный ключ', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Ключ выданный platon.ua для идентификации Клиента', 'woocommerce'),
                ),
                'password' => array(
                    'title' => __('Пароль', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Пароль выданный platon.ua участвующий в формировании MD5 подписи.', 'woocommerce'),
                )

            );
        }

        function is_valid_for_use()
        {

            if (!in_array(get_option('woocommerce_currency'), array('RUB', 'UAH', 'USD', 'EUR'))) {
                return false;
            }

            return true;
        }

        function process_payment($order_id)
        {
            $order = new WC_Order($order_id);
            return array(
                'result' => 'success',
                'redirect' => add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
            );

        }

        public function receipt_page($order)
        {
            echo '<p>' . __('Спасибо за Ваш заказ, пожалуйста, нажмите кнопку ниже, чтобы заплатить.', 'woocommerce') . '</p>';
            echo $this->generate_form($order);

        }


        public function generate_form($order_id)
        {
            global $woocommerce;
            global $sitepress;
//            $order_id += 3;
            $order = new WC_Order($order_id);
            $_SESSION['order_num'] = $order_id;
            $user_id = wp_get_current_user()->ID;

            $logger = wc_get_logger();
            $context = array( 'source' => 'platon_error' );
//
//            var_dump('etwwerew');
//            wp_die();
//            $order->update_status('wc-completed');


            $current_language = $sitepress->get_current_language();
//            $data['lg'] = '';
//
//            if ('ru' === $current_language) {
//                $data['lg'] = $current_language;
//            } elseif ('uk' === $current_language) {
//                $data['lg'] = $current_language;
//            } elseif ('en' === $current_language) {
//                $data['lg'] = $current_language;
//            }

            if (empty(get_user_meta($user_id, 'repeat_payment')) || get_user_meta($user_id, 'repeat_payment')[0] == 0) {
                $action_adr = $this->url;
                $result_url = str_replace('https:', 'http:', add_query_arg('wc-api', 'WC_Gateway_Platononline', home_url('/')));

                $pass = $this->password;
                $data['key'] = $this->secret;
                $data['url'] = $result_url;
                $data['data'] = base64_encode(json_encode(array('amount' => $order->order_total, 'name' => __('Оплата заказа №: ', 'woocommerce') . $order_id . __(' в магазине ', 'woocommerce') . home_url('/'), 'currency' => get_woocommerce_currency(), 'recurring')));
                $data['payment'] = 'CC';
                /* Calculation of signature */
                $sign = md5(strtoupper(strrev($data['key']) .
                    strrev($data['payment']) .
                    strrev($data['data']) .
                    strrev($data['url']) .
                    strrev($pass)
                ));


                return '<style> html {display: none !important;}</style>
                        <script type="text/javascript">jQuery(document).ready(function(){jQuery(\'#platon-form\').submit();});</script>
                        <form action="https://secure.platononline.com/payment/auth" method="post" id="platon-form">
                            <input type="hidden" name="payment" value="' . $data["payment"] . '" />
                            <input type="hidden" name="key" value="' . $data["key"] . '" />
                            <input type="hidden" name="url" value="' . $data["url"] . '" />
                            <input type="hidden" name="data" value="' . $data["data"] . '" />
                            <input type="hidden" name="sign" value="' . $sign . '" />
                            <input type="submit" class="button alt" value="Оплатить" />
                            <a class="button cancel" href="' . $order->get_cancel_order_url() . '">' . __("Отказаться от оплаты & вернуться в корзину", "woocommerce") . '</a>
                        </form> ';
            } else {
                $card = get_user_meta($user_id, 'card')[0];
                $client = wp_get_current_user()->ID;
                $url = 'https://secure.platononline.com/post/';
                $token = get_user_meta($user_id, 'rc_token')[0];
                $first = get_user_meta($user_id, 'first_trans_id')[0];
                $params['recurring_first_trans_id'] = $first;
                $params['recurring_token'] = $token;
                //}


                $params['action'] = 'RECURRING_SALE';
                $params['client_key'] = $this->secret; // sent to the e-mail client
                $CLIENT_PASS = $this->password; // client password
                $params['order_id'] = $order_id;
                $params['order_amount'] = $order->order_total;// сумма нового платежа
                $params['order_description'] = 'Оплата';
                $data['email'] = ''; // если нет в первой транзакции может быть пустым
                $card_number = $card;//из ответа, полученого на call-back url по первой транзакции, с поля 'card'
                $params['hash'] = md5(strtoupper(strrev($data['email']) .
                        $CLIENT_PASS .
                        strrev(substr($card_number, 0, 6) . substr($card_number, -4))
                    )
                );

                $tempData = array();
                foreach ($params as $key => $value) {
                    $tempData[] = $key . '=' . urlencode($value);
                }
                $crq = curl_init();

                curl_setopt($crq, CURLOPT_URL, $url);
                curl_setopt($crq, CURLOPT_HEADER, 1);
                curl_setopt($crq, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($crq, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($crq, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($crq, CURLOPT_POSTFIELDS, implode('&', $tempData));

                $result = curl_exec($crq);
                // if headers already send than hide form and redirect via js(in villa-woocommerce-function)
//                echo '<style> html {display: none !important;}</style>;';
//                var_dump($result);

                ob_start();
                var_dump($result);
                $result_debug = ob_get_clean();

                if ( strpos($result, 'SUCCESS') == true ) {
                    unset($_SESSION['order_num']);
                    $order->update_status('wc-completed');
                    $_SESSION['booking_success_pay'] = __('Thanks! Payment was successful.', 'villa');
                } elseif ( strpos($result, 'Order already exists') == true ) {
//                  need separate else for this case because in future parhapse need to fix this issue separately

                    $logger->critical(  wp_get_current_user()->user_email . ' - ' . $result_debug, $context );
                    wp_mail('VooDi.ua@gmail.com', 'Failed platon', wp_get_current_user()->user_email . ' - ' . $result_debug);
                } elseif (curl_errno($crq)) {
                    $logger->critical(  wp_get_current_user()->user_email . ' - ' . $result_debug, $context );
                    wp_mail('VooDi.ua@gmail.com', 'Failed platon', wp_get_current_user()->user_email . ' - ' . $result_debug);
                }  else {
                    $logger->critical(  wp_get_current_user()->user_email . ' - ' . $result_debug, $context );
                    wp_mail('VooDi.ua@gmail.com', 'Failed platon', wp_get_current_user()->user_email . ' - ' . $result_debug);
                }
                curl_close($crq);
            }
        }


        function check_ipn_response()
        {
            global $woocommerce;

            $woocommerce->cart->empty_cart();

            // we have situstion, when we received two request. One get, and post
            // we can get differend data; from get only user, and from post - post data
            // we need to combine this data
            if (isset($_GET["order"])) {
                if (get_option('platon_post') != false) {
                    $order = new WC_Order($_SESSION['order_num']);
                    update_user_platon_meta(wp_get_current_user()->ID, get_option('platon_post'));
                    unset($_SESSION['order_num']);
                    $_SESSION['booking_success_pay'] = __('Thanks! Payment was successful.', 'villa');
                    $order->update_status('wc-completed');
                } else {
                    update_option('platon_user', wp_get_current_user()->ID);
                    $order = new WC_Order($_SESSION['order_num']);
                    unset($_SESSION['order_num']);
                    $_SESSION['booking_success_pay'] = __('Thanks! Payment was successful.', 'villa');
                    $order->update_status('wc-completed');
                }

            }

            if (isset($_POST["order"])) {
                if (get_option('platon_user') != false) {
                    $order = new WC_Order($_SESSION['order_num']);
                    update_user_platon_meta(get_option('platon_user'), $_POST);
                    unset($_SESSION['order_num']);
                    $_SESSION['booking_success_pay'] = __('Thanks! Payment was successful.', 'villa');
                    $order->update_status('wc-completed');
                } else {
                    update_option('platon_post', $_POST);
                    $order = new WC_Order($_SESSION['order_num']);
                    unset($_SESSION['order_num']);
                    $_SESSION['booking_success_pay'] = __('Thanks! Payment was successful.', 'villa');
                    $order->update_status('wc-completed');
                }

            }

//
//            if ($order->status=='processing'){
//                $order->add_order_note(__('Платеж успешно оплачен через platon.ua', 'woocommerce'));
//            }


//            wp_redirect( $this->get_return_url( $order ));
//            wp_redirect( home_url() );

            exit;
        }
    }

    //
    function update_user_platon_meta($user_id, $post)
    {
        $options = ["card", 'sign', 'rc_id', 'rc_token', 'first_trans_id'];

        if (empty(get_user_meta($user_id, 'repeat_payment')) || get_user_meta($user_id, 'repeat_payment')[0] == 0) {
            update_user_meta($user_id, 'repeat_payment', 1);

            foreach ($options as $option) {
                if (!isset(get_user_meta($user_id, $option)[0])) {
                    if ($option == 'first_trans_id') { //id to first_trans_id mapping
                        update_user_meta($user_id, $option, $post['id']);
                    } else {
                        update_user_meta($user_id, $option, $post[$option]);
                    }
                }
            }
        }
    }

    function woocommerce_add_platononline_gateway($methods)
    {
        $methods[] = 'WC_Gateway_Platononline';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'woocommerce_add_platononline_gateway');

}