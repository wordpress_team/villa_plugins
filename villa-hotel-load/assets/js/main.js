jQuery(document).ready(function ($) {

    $(window).scroll(function () {
        var scroll = ($(this).scrollTop());
        var position = (scroll - 140 + 'px');
        var header = $('#table-header');
        if ($(this).scrollTop() > 140) {
            header.css('top', position);
        } else {
            header.css('top', 0);
        }

    });




    // var header = $("#table-header");
    // var scrollPrev = 0
    // $(window).scroll(function () {
    //     var scrolled = $(window).scrollTop();
    //     var firstScrollUp = false;
    //     var firstScrollDown = false;
    //     if (scrolled > 0) {
    //         if (scrolled > scrollPrev) {
    //             firstScrollUp = false;
    //             if (scrolled < header.height() + header.offset().top) {
    //                 if (firstScrollDown === false) {
    //                     var topPosition = header.offset().top;
    //                     header.css({"top": topPosition + "px"});
    //                     firstScrollDown = true;
    //                 }
    //                 header.css({"position": "absolute"});
    //             } else {
    //                 header.css({"position": "fixed", "top": "-" + header.height() + "px"});
    //             }
    //         } else {
    //             firstScrollDown = false;
    //             if (scrolled > header.offset().top) {
    //                 if (firstScrollUp === false) {
    //                     var topPosition = header.offset().top;
    //                     header.css({"top": topPosition + "px"});
    //                     firstScrollUp = true;
    //                 }
    //                 header.css({"position": "absolute"});
    //             } else {
    //                 header.removeAttr("style");
    //             }
    //         }
    //         scrollPrev = scrolled;
    //     }
    // });





    // var hellopreloader = document.getElementById("hellopreloader");
    // hellopreloader.style.display = "none";


    // $('body').on('click' , '.open_booking_load' , function() {
    //     hellopreloader.style.display = "block";
    // });

    $('body').on('focus', '.cmb2-datepicker', function(){
        $(this).datepicker();
    });

    var modal =$('#hotel_loud_modal');
    $(window).on('click', function (e) {
        if (e.target.id == ('hotel_loud_modal')) {
            modal.hide();
        }
    });
    // Get the modal
    var modal = $('#hotel_loud_modal');

// When the user clicks on the button, open the modal
    $('body').on('click', '.open_booking_load', function () {
        modal.show();
    });

    // When the user clicks on <span> (x), close the modal
    $('body').on('click', '.close', function () {
        modal.hide();
    });

    $('#startDate').datepicker({
        // startDate: new Date(),
        zIndexOffset: 60,
        format: 'dd.mm.yyyy',
        weekStart: 1,
        autoclose: true,
        // language: language.key1
    });

    $('#endDate').datepicker({
        startDate: new Date(),
        zIndexOffset: 60,
        format: 'dd.mm.yyyy',
        weekStart: 1,
        autoclose: true,
        // language: language.key1
    });

    $('#startDate').on('change', function () {
        var end = $('#endDate').datepicker('getDate');
        var start = $(this).datepicker('getDate');
        var startDate = new Date(start).setHours(0, 0, 0, 0);
        var endDate = new Date(end).setHours(0, 0, 0, 0);

        if (startDate >= endDate) {
            $("#endDate").datepicker("setDate", new Date(startDate + 86400000));
        }
    });


    $('#endDate').on('change', function () {
        var start = $('#startDate').datepicker('getDate');
        var startDate = new Date(start).setHours(0, 0, 0, 0);
        var end = $(this).datepicker('getDate');
        var endDate = new Date(end).setHours(0, 0, 0, 0);
        var currentDate = new Date().setHours(0, 0, 0, 0);

        if (currentDate == endDate) {
            $('#startDate').datepicker('setDate', new Date(currentDate));
            $("#endDate").datepicker("setDate", new Date(currentDate + 86400000));
            return false;
        }
        if (startDate >= endDate) {
            $('#startDate').datepicker('setDate', new Date(endDate - 86400000));
        }
    });


    // $('#loadingDatesDiv .datepick').datepicker({
    //     startDate: new Date(),
    //     zIndexOffset: 60,
    //     format: 'dd.mm.yyyy',
    //     weekStart: 1
    // });



    // draw bookings
    $.ajax({
        url : villa_hotel_load_ajax.url,
        method : 'POST',
        data : {
            action     : 'villa_get_bookings',
            nonce_code : villa_hotel_load_ajax.nonce,
            date_from  : $('#startDate').val(),
            date_to    : $('#endDate').val(),
            service    : $('#service').val(),
        },
        success : function (response) {


            draw_bookings(JSON.parse(response));
            // console.log(JSON.parse(response));

            /*$(".villa-load-table a").each(function() {

		        var booking_room_id = $(this).data("room_id");
		        var booking_start   = $(this).data("start");
		        var booking_id      = $(this).data("booking-id");

				$(".villa-load-table p.ralative_block").each(function() {

					var room_start   = $(this).data("start");
        			var room_room_id = $(this).data("room");

        			if ( room_start == booking_start && room_room_id == booking_room_id) {

	        			$(this).html('<span class="booking-id">'+booking_id+'</span>');
	        		}
				});
		    });*/

        }
    });



    $('#villa-load-table-ajax').on('click', function (e){
        $.ajax({
            url : villa_hotel_load_ajax.url,
            method : 'POST',
            data : {
                action     : 'villa_load_table',
                nonce_code : villa_hotel_load_ajax.nonce,
                date_from  : $('#startDate').val(),
                date_to    : $('#endDate').val(),
                service    : $('#service').val(),
            },
            success : function (response) {
                // console.log(response);
                $('#villa-load-table').replaceWith(response);


                $.ajax({
                    url : villa_hotel_load_ajax.url,
                    method : 'POST',
                    data : {
                        action     : 'villa_get_bookings',
                        nonce_code : villa_hotel_load_ajax.nonce,
                        date_from  : $('#startDate').val(),
                        date_to    : $('#endDate').val(),
                        service    : $('#service').val(),
                    },
                    success : function (response) {


                        draw_bookings(JSON.parse(response));
                        // console.log(JSON.parse(response));
                    }
                });
                $('#hellopreloader').css('display', 'none');
            }
        });



    });
    // need two ajax call
    // first update meta, second show form
    // this manipulation need couse function php load per every ajax call and firstly,
    // than ajax handler. Considering that we need to update meta before reading function
    // php we need two ajax
    $('body').on('click', '.open_booking_load', function () {
        $('#villa_booking_modal_form').attr( 'data_booking_id', $(this).attr('id') );
        $.ajax({
            url : villa_hotel_load_ajax.url,
            method : 'POST',
            data : {
                action     : 'villa_update_id_option',
                nonce_code : villa_hotel_load_ajax.nonce,
                villa_admin_booking_id : $(this).attr('id'),
            },
            success : function (response) {
                $.ajax({
                    url : villa_hotel_load_ajax.url,
                    method : 'POST',
                    data : {
                        action     : 'villa_get_booking_form_by_id',
                        nonce_code : villa_hotel_load_ajax.nonce,
                    },
                    success : function (response) {
                        // $('.villa_remodal').html(response);
                        hellopreloader.style.display = "none";


                        var temp_div = document.createElement('div');
                        temp_div.innerHTML = response;
                        $('#cmb2-metabox-booking_metabox').html(temp_div.querySelector('#cmb2-metabox-booking_metabox').innerHTML);

                        var cmb_form = $('.cmb-form');
                        if ( cmb_form.find('input[type=hidden]').length ) {
                            cmb_form.find('input[type=hidden]').remove();
                        }

                        var services_total_price = 0;
                        $.each($('.services_price'), function () {
                            if (($(this).val()) !== '') {
                                services_total_price += parseInt($(this).val());
                            }
                        });
                        var transfer_total_price = 0;
                        $.each($('.transfer_price'), function () {
                            if (($(this).val()) !== '') {
                                transfer_total_price += parseInt($(this).val());
                            }
                        });
                        var excursions_total_price = 0;
                        $.each($('.excursions_price'), function () {
                            if (($(this).val()) !== '') {
                                excursions_total_price += parseInt($(this).val());
                            }
                        });
                        var room_price = parseInt($('#booking_price').val());

                        $('#villa_booking_cost_total').val(services_total_price + excursions_total_price + transfer_total_price + room_price);
                        var room_paid = $('#booking_room_paid').attr("checked");
                        var transfer_paid = $('#booking_transfer_paid').attr("checked");
                        var services_paid = $('#booking_services_paid').attr("checked");
                        var excursions_paid = $('#booking_excursion_paid').attr("checked");
                        var total_price = parseInt($('#villa_booking_cost_total').val());
                        $('#villa_booking_cost_tobepaid').val(total_price);
                        var total_to_pay = parseInt($('#villa_booking_cost_tobepaid').val());
                        if (room_paid) {
                            total_to_pay = total_to_pay - room_price;
                            $('#villa_booking_cost_tobepaid').val(total_to_pay);
                        }

                        if (transfer_paid) {
                            total_to_pay = total_to_pay - transfer_total_price;
                            $('#villa_booking_cost_tobepaid').val(total_to_pay);

                        }
                        if (services_paid) {
                            total_to_pay = total_to_pay - services_total_price;
                            $('#villa_booking_cost_tobepaid').val(total_to_pay);

                        }

                        if (excursions_paid) {
                            total_to_pay = total_to_pay - excursions_total_price;
                            $('#villa_booking_cost_tobepaid').val(total_to_pay);

                        }
                        $('body').on('change', '#booking_room_paid', function () {
                            if ($('#booking_room_paid').is(':checked')) {
                                total_to_pay = total_to_pay - room_price;
                                $('#villa_booking_cost_tobepaid').val(total_to_pay);
                            }
                        });
                        $('body').on('change', '#booking_transfer_paid', function () {
                            if ($('#booking_transfer_paid').is(':checked')) {
                                total_to_pay = total_to_pay - transfer_total_price;

                                $('#villa_booking_cost_tobepaid').val(total_to_pay);

                            }
                        });
                        $('body').on('change', '#booking_services_paid', function () {
                            if ($('#booking_services_paid').is(':checked')) {
                                total_to_pay = total_to_pay - services_total_price;
                                $('#villa_booking_cost_tobepaid').val(total_to_pay);

                            }
                        });
                        $('body').on('change', '#booking_excursion_paid', function () {
                            if ($('#booking_excursion_paid').is(':checked')) {
                                total_to_pay = total_to_pay - excursions_total_price;
                                $('#villa_booking_cost_tobepaid').val(total_to_pay);

                            }
                        });

                    }
                });
            }
        });
    });


    $('.booking_modal_submit_button').on('click', function (e) {
        e.preventDefault();

        $.ajax({
            url : villa_hotel_load_ajax.url,
            method : 'POST',
            data : {
                action     : 'villa_update_booking',
                nonce_code : villa_hotel_load_ajax.nonce,
                post_data  : $('#villa_booking_modal_form').serialize(),
                booking_id : $('#villa_booking_modal_form').attr( 'data_booking_id' )
            },
            success : function (response) {
                // alert(response);
            }
        });

    });


// draw_bookings(villa_hotel_load_ajax.bookings);

    function draw_bookings(bookings_array) {
        var additionalData = [];
        additionalData['recHeight'] = 49;
        additionalData['recWidth'] = 75;
        // additionalData['background'] = '#ccc;';
        // additionalData['villaTable'] = $('#villa-load-table');
        additionalData['room_number_width'] = 100;
        additionalData['bad_places'] = 130;
        // var status = 89;
        additionalData['left_offset'] = additionalData['room_number_width'] + additionalData['bad_places'];
        additionalData['move'] = '';


        // console.log(villa_hotel_load_ajax.bookings);

        $.each(bookings_array, function(index, value) {
            var booking_arrival_date = value['booking_arrival_date'];
            var booking_departure_date = value['booking_departure_date'];
            var link = value['link'];
            var room = value['room'];
            var days_from_start = value['days_from_start'];
            var room_title = value['room_title'];
            var id = value['id'];
            var old_bookings = '';


            if ( value['old_booking'] ) {

                for ( var i = 0; i < value['old_booking'].length; i++ ) {
                    var old_room = value['old_booking'][i]['villa_old_booking_room'];
                    var old_next_room;
                    var villa_old_booking_arrival = value['old_booking'][i]['villa_old_booking_arrival'];
                    var villa_old_booking_departure = value['old_booking'][i]['villa_old_booking_departure'];


                    if ( villa_old_booking_departure ) { // check if we have this data for proper drawing
                        if ( i == value['old_booking'].length - 1 ) {
                            $('.villa-info-booking').append( getBookingHtml(villa_old_booking_arrival, villa_old_booking_departure, link, old_room, days_from_start, id, additionalData, room_title ) );
                            break;
                        }
                        old_next_room = value['old_booking'][i+1]['villa_old_booking_room'];

                        $('.villa-info-booking').append( getBookingHtml(villa_old_booking_arrival, villa_old_booking_departure, link, old_room, days_from_start, id, additionalData, value['old_room_mappings'][old_next_room] ) );
                    }



                }

            }


            $('.villa-info-booking').append( getBookingHtml(booking_arrival_date, booking_departure_date, link, room, days_from_start, id, additionalData ) );

        });
    }


    function checkDate(booking_arrival_date, booking_departure_date) {
        var end = $('#endDate').datepicker('getDate');
        var start = $('#startDate').datepicker('getDate');

        if ( !((parseDate(booking_arrival_date) <= end) && (parseDate(booking_departure_date) >= start) ) ) {
            return false;
        }
        return true;
    }



    function getBookingHtml(booking_arrival_date, booking_departure_date, link, room, days_from_start, id, additionalData, old_room=''  ) {
        // if booking dont in range return
        if ( !checkDate(booking_arrival_date, booking_departure_date) ) {
            return '';
        }

        var start = $('#startDate').datepicker('getDate');
        var startDate = new Date(start);
        var top;
        var left;
        var width;
        var small_class = '';

        var duration = daydiff(parseDate( booking_arrival_date ),  parseDate( booking_departure_date ) );
        var days_from_start = daydiff(startDate,  parseDate( booking_arrival_date ) );

        if ( old_room ) {
            old_room = 'переехал в ' + old_room;
        }

        if ( days_from_start <= 0 ) {
            duration = daydiff(startDate,  parseDate( booking_departure_date ) );
        }

        left = ( ( days_from_start * additionalData['recWidth'] ) + additionalData['left_offset'] + ( additionalData['recWidth'] / 2 ) ) + 'px;';
        width = ( (duration * additionalData['recWidth']) ) + 'px ;';


        if ((duration * additionalData['recWidth']) <= 150) {
            small_class = 'bookingDiv-sm';
        }

        // $('.bookingDiv-sm').on('mouseenter', function(){
        //     // ($(this).clone().addClass('bookingDiv-sm-clone').appendTo(this));
        //     ($(this).clone().addClass('bookingDiv-sm-clone').appendTo('.villa-info-booking'));
        //     $('.bookingDiv-sm-clone').addClass('active');
        //     console.log('view');
        //
        //
        //     $('.bookingDiv-sm-clone.active').on('mouseenter', function(){
        //         // $('.bookingDiv-sm').mouseenter();
        //         // $('.bookingDiv-sm-clone.active').addClass('active').show();
        //         console.log('show');
        //     });
        //     //
        //     // $('.bookingDiv-sm-clone.active').on('mouseout', function() {
        //     //     $('.bookingDiv-sm').mouseout();
        //     //     // $('.bookingDiv-sm-clone.active').removeClass('active').hide();
        //     //     console.log('hide');
        //     // });
        //
        // });
        //
        //
        // $('.bookingDiv-sm').on('mouseover', function() {
        //     $('.bookingDiv-sm-clone').removeClass('active')
        //     console.log('hide');
        // });



        $.each($('.room_title'), function() {
            if ( $(this).attr('data-attr') == room ) {
                top = ( $(this).index() ) * additionalData['recHeight'];
            }
        });

        if ( days_from_start < 0 ) {
            left = additionalData['left_offset'] + 'px;';
            width = ( duration * additionalData['recWidth'] + additionalData['recWidth'] / 2 )  + 'px;';
        }
        var date_string = booking_arrival_date + "-" + booking_departure_date;
        // var booking_style = 'left:' + left + 'top: ' + top + 'px;' + 'width:' + width + 'height:' +  additionalData['recHeight']  + 'px;';
        var booking_style = 'left:' + left + 'top: ' + top + 'px;' + 'width:' + width;

        return '<a data-room_id="'+room+'" data-booking-id="'+id+'" data-start="'+days_from_start+'" target="_blank" href="' + link + '" class="bookingDiv ' + small_class + '" style="' + booking_style + '"> ' +
                    '<div class="info-booked-person green">' +
                        '<div class="name-pesron">' + id + '</div>' +
                        '<div class="date-string">( ' + date_string + ' ) ' + old_room + '</div>' +
                        '<div class="status-order"></div>' +
                    '</div>' +
                    '<div data-room_id="'+room+'" data-booking-id="'+id+'" data-start="'+days_from_start+'" target="_blank" href="' + link + '" class="bookingDiv bookingDivTooltip ' + small_class + '" style="' + booking_style + '"> ' +
                        '<span class="info-booked-person green">' +
                            // '<span>show</span>' +
                            '<span class="name-pesron">' + id + '</span>' +
                            '<span class="date-string">( ' + date_string + ' ) ' + old_room + '</span>' +
                            '<span class="status-order"></span>' +
                        '</span>' +
                    '</div>' +
                '</a>';

    }


    function parseDate(str) {
        var mdy = str.split('.');
        return new Date(mdy[2], mdy[1]-1, mdy[0]);
    }

    function daydiff(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }

    /*$(".villa-load-table p.ralative_block").hover(function() {

        var room_id = $(this).data("room");
        var start   = $(this).data("start");
        $(".villa-load-table a").hide();

        $(".villa-load-table a").each(function() {

            var booking_room_id = $(this).data("room_id");
            var booking_start   = $(this).data("start");

            if ( booking_room_id == room_id && booking_start == start ) {

                $(this).show();
            }
        });
    });*/


});











