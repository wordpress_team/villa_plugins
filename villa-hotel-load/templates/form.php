<?php
if(is_null($this) || !($this instanceof VillaHotelLoad))
    return
?>

<?php 
$date      = new DateTime('-6 days');
$dateStart = $date->format('d.m.Y'); 
$date      = new DateTime('+30 days');
$dateEND   = $date->format('d.m.Y'); 
?>
    
<!--TODO add services-->
<div id="loadingDatesDiv">
  <!-- <p class="text-info">Для просмотра детализации кликните левой кнопкой мышки по закрашенному сектору</p> -->
  <div class="form-block">
      <label for="startDate">Показать с</label>
      <!-- <input type="text" class="datepick hasDatepicker" id="startDate" value="<?php //echo $this->date_from->format('d.m.Y');?>"> -->
      <input type="text" class="datepick hasDatepicker" id="startDate" value="<?php echo $dateStart; ?>">
      <label for="endDate">по</label>                                         
      <input type="text" class="datepick hasDatepicker" id="endDate" value="<?php echo $dateEND; ?>">
      <select class="new-option" name="service" id="service">
          <option value="0" <?php echo empty($_POST['service']) ? 'selected' : ''; ?>>Все услуги</option>
          <?php foreach ($this->services as $service){ ?>
              echo '<option value="<?php echo $service->ID; ?>" <?php echo $service->ID == $_POST['service'] ? 'selected' : ''; ?>><?php echo $service->post_title; ?></option>';
          <?php } ?>
      </select>

      <a class="button button-primary" id="villa-load-table-ajax">Фильтровать</a>
  </div>
  <!-- /.form-block -->

</div>