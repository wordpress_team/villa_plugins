<?php
if (is_null($this) || !($this instanceof VillaHotelLoad))
    return
        $modal_post_id = 2;
?>
<!-- <div id="hotel_loud_modal" class="modal fade" role="dialog">
    <div class="modal-content-top">

        <button class="close" data-dismiss="modal">&times;</button>
        <form class="modal-content" action="<?php //echo esc_url(admin_url('admin-post.php')) ?>"
              id="villa_booking_modal_form" data_booking_id=""
              class="cmb-form" method="post" id="booking_metabox" enctype="multipart/form-data"
              encoding="multipart/form-data">

            <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                <div id="booking_metabox" class="postbox  cmb2-postbox no_border">
                    <div class="inside">
                        <div class="cmb2-wrap form-table">
                            <div id="cmb2-metabox-booking_metabox" class="cmb2-metabox cmb-field-list">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="submit" value="Сохранить" class="booking_modal_submit_button" data-dismiss="modal">
            <input data-remodal-action="close" type="submit" value="Сохранить и закрыть"
                   class="booking_modal_submit_button close"  data-dismiss="modal">

        </form>
    </div>
</div> -->
<div id="villa-load-table" class="villa-load-table">
    <!-- <div class="bookingDiv"></div> -->
    <div id="table-header" class='table-load-items'>

        <?php if (!empty($_POST['service'])) { ?>
            <div>
                <div>Кол-во гостей</div>
                <?php foreach ($this->period_plus_one as $dt) {
                    echo '<th>' . $this->count_service_customers_by_date($dt) . ' (' . $this->count_service_children_by_date($dt) . ')</th>';
                } ?>
            </div>
        <?php } ?>
        <div class='villa-load-info'>
            <p class=villa-hotel-rooms>Комнаты</p>
            <p class='villa-hotel-sleepeing-area'>Спальных мест</p>
            <!-- <th>Статус</th> -->
            <div  class='villa-load-date'>
                <?php foreach ($this->period_plus_one as $dt) {
                    echo '<p>' . $this->change_date_language_to_ru( $dt->format("D d.m") ) . '</p>';
                } ?>
            </div>
        </div>
    </div>
    <div class='villa-info-booking'>
        <?php /*Start to draw row*/
        foreach ($this->rooms as $room) { ?>

            <div class="room_title" data-attr = <?php echo $room->ID;  ?> >
                <?php $terms = wp_get_post_terms( $room->ID, 'villa_room_type' ); ?>
                <p class='quantity-rooms'><?php echo $room->post_title; ?></p>
                <p class='type-rooms'> <?php echo $terms[0]->name; ?></p>
                <!-- <td class='room-status status-bad'>Dirty</td> -->
                <?php /*Start to draw column*/

                $roomDates = iterator_to_array( $this->period_plus_one );
                for ( $i = 0; $i < count($roomDates); $i++ ) {
                    $date = $roomDates[$i]; ?>

                    <p class="ralative_block" data-room="<?php echo $room->ID; ?>" data-start="<?php echo $date->format("d") - 1; ?>">

                        <!-- <a target="_blank" href="#" class="booking-div">
                        <span class="booking-div__name"><?php //echo $i; ?></span><br>
                        <span class="booking-div__data">( 08.03.2018-09.03.2018 )</span>
                    </a> -->

                    </p>

                <?php } ?>
            </div>
        <?php } ?>

    </div>
</div>



<!-- <div id="hellopreloader">
    <div id="hellopreloader_preload">
        <p class="preloader">Подождите, идет загрузка.</p>
    </div>
</div> -->