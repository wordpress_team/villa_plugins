<?php
/*
Plugin Name: Villa hotel load
Description: Show rooms load table for villa la scala hotel
Version: 0.1
Author: Vazgen Malhasian <malhasianv@gmail.com>
*/

// require_once( ABSPATH . "wp-includes/pluggable.php" );

class VillaHotelLoad {
//    TODO: add service property, and substitute $_POST['service'] var with it

    protected static $single_instance = null;

    protected $path;
    protected $rooms;
    protected $bookings;
    protected $services;
    protected $mapped_booking_array;
    protected $ajax_booking_array;
    protected $date_from;
    protected $date_to;
    protected $date_to_plus_one;
    protected $period;
    protected $period_plus_one;


    /**
     * Creates or returns an instance of this class.
     * @return VillaHotelLoad A single instance of this class.
     */
    public static function get_instance() {
        if ( null === self::$single_instance ) {
            self::$single_instance = new self();
        }

        return self::$single_instance;
    }
    protected function __construct() {
//        get path to plugin directory
        $this->path = plugin_dir_url( __FILE__ );

        if($_GET['page'] == 'villa_hotel_load') {
            add_action('admin_enqueue_scripts', array($this, 'load_scripts'));
        }
        if( wp_doing_ajax() ) {
            add_action('wp_ajax_villa_load_table', array($this, 'load_table'));
            add_action('wp_ajax_nopriv_villa_load_table', array($this, 'load_table'));

            add_action('wp_ajax_villa_get_bookings', array($this, 'get_bookings'));
            add_action('wp_ajax_nopriv_villa_get_bookings', array($this, 'get_bookings'));
        }

//        if($_GET['page'] == 'villa_hotel_load') {

            $this->rooms = get_posts(array(
                'numberposts' => -1,
                'orderby' => 'title',
                'order' => 'ASC',
                'post_type' => 'villa_hotel_room',
                'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
            ));

            /*$this->services = get_posts(array(
                'post_type' => 'product',
                'meta_key' => 'product_type',
                'meta_value' => 'services',
                'posts_per_page' => -1,
                'suppress_filters' => false
            ));*/

            $args = array(
                'post_type' => 'product',
                'meta_key' => 'product_type',
                'meta_value' => 'services',
                'posts_per_page' => -1,
                'suppress_filters' => false
            );

            $query = new WP_Query($args);
            $posts = $query->posts;

            foreach ($posts as $post) {

                $this->services[] = $post;
            }

            wp_reset_postdata();

//        }

        $this->set_date_properties();
//        if($_GET['page'] == 'villa_hotel_load') {

//            $this->set_booking_arrays();
//        }

        add_action( 'admin_menu', array( $this, 'register_menu_pages') );
    }

    public function register_menu_pages(){
        add_menu_page(
            __('Загрузка отеля', 'villa'), __('Загрузка отеля', 'villa'), 'manage_options', 'villa_hotel_load', array( $this, 'render_main_page'), 'dashicons-chart-bar', 6
        );
    }

    public function set_date_properties(){

        $date      = new DateTime('-6 days');
        $dateStart = $date->format('d.m.Y');
        $date      = new DateTime('+30 days');
        $dateEND   = $date->format('d.m.Y');

        // set date period manually to current month if it wasn't passed
        $this->date_from = new DateTime( is_null($_POST['date_from']) ? $dateStart : $_POST['date_from'] );
        // $this->date_from = new DateTime( is_null($_POST['date_from']) ? date('01.m.Y') : $_POST['date_from'] );
        // $this->date_to = new DateTime( is_null($_POST['date_to']) ? date('t.m.Y') : $_POST['date_to'] );
        $this->date_to = new DateTime( is_null($_POST['date_to']) ? $dateEND : $_POST['date_to'] );
        $this->date_to_plus_one = $this->date_to->add(new DateInterval('P1D'));

        $interval = DateInterval::createFromDateString('1 day');

        $this->period = new DatePeriod($this->date_from, $interval, $this->date_to);
        $this->period_plus_one = new DatePeriod($this->date_from, $interval, $this->date_to_plus_one);
    }

    public function change_date_language_to_ru($date){

        $date = str_replace('Sun','Вс',$date);
        $date = str_replace('Mon','Пн',$date);
        $date = str_replace('Tue','Вт',$date);
        $date = str_replace('Wed','Ср',$date);
        $date = str_replace('Thu','Чт',$date);
        $date = str_replace('Fri','Пт',$date);
        $date = str_replace('Sat','Сб',$date);

        return $date;
    }

    private function count_service_customers_by_date(DateTime $date){
        return $this->count_service_adults_by_date($date) + $this->count_service_children_by_date($date);
    }

    private function count_service_adults_by_date(DateTime $date){
        $adults = 0;

        foreach($this->mapped_booking_array[$date->format("d.m.Y")] as $booking){
            foreach (get_post_meta($booking->ID, 'booking_services_repeat_group', true) as $booking_service){
                if($booking_service['villa_services_select'] == $_POST['service']){
                    $i = 1;
                    while(isset($booking_service['checkbox_adult_' . $i])){
                        if(isset($booking_service['date_checkbox_adult_' . $i . '_' . $date->format("d.m.Y")])){
                            $adults++;
                        }
                        $i++;
                    }
                }
            }
        }

        return $adults;
    }

    private function count_service_children_by_date(DateTime $date){
        $children = 0;

        foreach($this->mapped_booking_array[$date->format("d.m.Y")] as $booking){
            foreach (get_post_meta($booking->ID, 'booking_services_repeat_group', true) as $booking_service){
                if($booking_service['villa_services_select'] == $_POST['service']){
                    $i = 1;
                    while(isset($booking_service['checkbox_child_' . $i])){
                        if(isset($booking_service['date_checkbox_child_' . $i . '_' . $date->format("d.m.Y")])){
                            $children++;
                        }
                        $i++;
                    }
                }
            }
        }

        return $children;
    }

//    private function set_booking_arrays(){
//        $this->bookings = get_posts(array(
//            'numberposts' => -1,
//            'orderby'     => 'title',
//            'order'       => 'ASC',
//            'post_type'   => 'villa_booking',
//            // 'meta_query' => array(
//            //     'relation' => 'AND',
//            //     array(
//            //         'key'       => 'booking_arrival_date',
//            //         'value'     => $this->date_to->format('d.m.Y'),
//            //         'compare'   => '<',
//            //     ),
//            //     array(
//            //         'key'       => 'booking_departure_date',
//            //         'value'     => $this->date_from->format('d.m.Y'),
//            //         'compare'   => '>',
//            //     ),
//            // ),
//            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
//        ));
//
////        create array keyed by dates in period with arrays of links to booking posts as values
//        foreach($this->bookings as $booking_key => $booking) {
//
//            $booking_arrival_date = strtotime(get_post_meta($booking->ID, 'booking_arrival_date', true));
//            $booking_departure_date = strtotime(get_post_meta($booking->ID, 'booking_departure_date', true));
//
//            if (($booking_arrival_date <= strtotime($this->date_to->format('d.m.Y'))) && ($booking_departure_date >= strtotime($this->date_from->format('d.m.Y')))) {
//
//                $booking_begin = new DateTime( get_post_meta($booking->ID, 'booking_arrival_date', true) );
//                $booking_end = new DateTime( get_post_meta($booking->ID, 'booking_departure_date', true) );
//                $booking_end = $booking_end->add(new DateInterval('P1D'));
//
//                $booking_interval = DateInterval::createFromDateString('1 day');
//                $booking_period = new DatePeriod($booking_begin, $booking_interval, $booking_end);
//
//                foreach ( $booking_period as $day ){
//                    $this->mapped_booking_array[$day->format( "d.m.Y" )][]=&$this->bookings[$booking_key];
//                }
//            }
//        }
//
//        foreach($this->bookings as $booking_key => $booking) {
//
//            $booking_arrival_date = get_post_meta($booking->ID, 'booking_arrival_date', true);
//            $booking_departure_date = get_post_meta($booking->ID, 'booking_departure_date', true);
//            $less_then_search = false;
//
//            if ((strtotime($booking_arrival_date) <= strtotime($this->date_to->format('d.m.Y'))) && (strtotime($booking_departure_date) >= strtotime($this->date_from->format('d.m.Y')))) {
//
//
//                // $booking_begin = new DateTime( get_post_meta($booking->ID, 'booking_arrival_date', true) );
//                // $booking_end = new DateTime( get_post_meta($booking->ID, 'booking_departure_date', true) );
//                // $booking_end = $booking_end->add(new DateInterval('P1D'));
//
//                // $booking_interval = DateInterval::createFromDateString('1 day');
//                // $booking_period = new DatePeriod($booking_begin, $booking_interval, $booking_end);
//                // $days_from_start = iterator_count( new DatePeriod( $this->date_from, $booking_interval, $booking_begin) );
//                // $booking_duration = iterator_count($booking_period);
//
//                // foreach ( $booking_period as $day ) {
//
//                // if ( $days_from_start == 0 ) {
//                //     $booking_duration = iterator_count( new DatePeriod( $this->date_from, $booking_interval, $booking_end) );
//                // }
//
//                // if ( strtotime($this->date_from->format('d.m.Y') ) > strtotime( $booking_arrival_date )) {
//                //     $less_then_search = true;
//                // }
//
//                $this->ajax_booking_array[$booking->ID]['link'] = admin_url( 'post.php?post=' . $booking->ID ) . '&action=edit';
//                $this->ajax_booking_array[$booking->ID]['id'] = $booking->ID;
//                // $this->ajax_booking_array[$booking->ID]['duration'] = $booking_duration;
//                // $this->ajax_booking_array[$booking->ID]['arrival'] = $booking_arrival_date;
//                // $this->ajax_booking_array[$booking->ID]['days_from_start'] = $days_from_start;
//                // $this->ajax_booking_array[$booking->ID]['departure'] = $booking_departure_date;
//                // $this->ajax_booking_array[$booking->ID]['less_then_search'] = $less_then_search;
//
//                $this->ajax_booking_array[$booking->ID]['old_booking'] = get_post_meta($booking->ID, 'villa_old_booking', true);
//                $roomPost = get_post( get_post_meta($booking->ID, 'booking_room', true) );
//                $roomTitle = $roomPost->post_title;
//                $old_room_mappings = [];
//
//                // foreach ( $this->ajax_booking_array[$booking->ID]['old_booking'] as $old_boking  ) {
//                //     $oldId = intval ( $old_boking['villa_old_booking_room'] );
//                //     // TODO delete this checking. Fix repeater deleting
//                //     if ( $oldId != 0 ) {
//                //         $oldRoomPost = get_post( $oldId );
//                //         $oldRoomTitle = $oldRoomPost->post_title;
//                //         $old_room_mappings[ $oldId ] = $oldRoomTitle;
//                //     }
//                // }
//
//                $this->ajax_booking_array[$booking->ID]['room'] = get_post_meta($booking->ID, 'booking_room', true);
//                $this->ajax_booking_array[$booking->ID]['room_title'] = $roomTitle;
//
//                $this->ajax_booking_array[$booking->ID]['booking_arrival_date'] = $booking_arrival_date;
//                $this->ajax_booking_array[$booking->ID]['booking_departure_date'] = $booking_departure_date;
//
//                $this->ajax_booking_array[$booking->ID]['old_room_mappings'] = $old_room_mappings;
//
//            }
//
//
//        }
//    }

    public function get_bookings() {
        $this->bookings = get_posts(array(
            'numberposts' => -1,
            'orderby'     => 'title',
            'order'       => 'ASC',
            'post_type'   => 'villa_booking',
            // 'meta_query' => array(
            //     'relation' => 'AND',
            //     array(
            //         'key'       => 'booking_arrival_date',
            //         'value'     => $this->date_to->format('d.m.Y'),
            //         'compare'   => '<',
            //     ),
            //     array(
            //         'key'       => 'booking_departure_date',
            //         'value'     => $this->date_from->format('d.m.Y'),
            //         'compare'   => '>',
            //     ),
            // ),
            'suppress_filters' => true, // подавление работы фильтров изменения SQL запроса
        ));




        foreach($this->bookings as $booking_key => $booking) {

            $booking_arrival_date = get_post_meta($booking->ID, 'booking_arrival_date', true);
            $booking_departure_date = get_post_meta($booking->ID, 'booking_departure_date', true);
            $less_then_search = false;

            if ((strtotime($booking_arrival_date) <= strtotime($this->date_to->format('d.m.Y'))) && (strtotime($booking_departure_date) >= strtotime($this->date_from->format('d.m.Y')))) {


                $booking_begin = new DateTime( get_post_meta($booking->ID, 'booking_arrival_date', true) );
                $booking_end = new DateTime( get_post_meta($booking->ID, 'booking_departure_date', true) );
                // $booking_end = $booking_end->add(new DateInterval('P1D'));

                $booking_interval = DateInterval::createFromDateString('1 day');
                $booking_period = new DatePeriod($booking_begin, $booking_interval, $booking_end);
                $days_from_start = iterator_count( new DatePeriod( $this->date_from, $booking_interval, $booking_begin) );
                $booking_duration = iterator_count($booking_period);
                $link = admin_url( 'post.php?post=' . $booking->ID ) . '&action=edit';

                // foreach ( $booking_period as $day ) {

                if ( $days_from_start == 0 ) {
                    $booking_duration = iterator_count( new DatePeriod( $this->date_from, $booking_interval, $booking_end) );
                }

                if ( strtotime($this->date_from->format('d.m.Y') ) > strtotime( $booking_arrival_date )) {
                    $less_then_search = true;
                }

                $this->ajax_booking_array[$booking->ID]['link'] = $link;
                $this->ajax_booking_array[$booking->ID]['id'] = $booking->ID;
                $this->ajax_booking_array[$booking->ID]['duration'] = $booking_duration;
                // $this->ajax_booking_array[$booking->ID]['arrival'] = $booking_arrival_date;
                $this->ajax_booking_array[$booking->ID]['days_from_start'] = $days_from_start;
                // $this->ajax_booking_array[$booking->ID]['departure'] = $booking_departure_date;
                $this->ajax_booking_array[$booking->ID]['less_then_search'] = $less_then_search;

                $this->ajax_booking_array[$booking->ID]['booking_arrival_date'] = $booking_arrival_date;
                $this->ajax_booking_array[$booking->ID]['booking_departure_date'] = $booking_departure_date;

                $this->ajax_booking_array[$booking->ID]['room'] = get_post_meta($booking->ID, 'booking_room', true);






                // }
            }
        }

        echo  json_encode($this->ajax_booking_array);


        wp_die();

    }

    public function load_scripts() {
        wp_enqueue_script('villa_hotel_load_main', $this->path . 'assets/js/main.js', array(), '', true );
        wp_enqueue_script('villa_datepicker_js', $this->path . 'assets/js/bootstrap-datepicker.js', array(), '', true);
        wp_localize_script('villa_hotel_load_main', 'villa_hotel_load_ajax',
            array(
                'url' => admin_url('admin-ajax.php'),
                'nonce' => wp_create_nonce('villa-hotel-load-nonce'),
                'bookings' => $this->ajax_booking_array
            )
        );
        wp_enqueue_style('villa_hotel_load_bootstrap-datepicker3.css', $this->path . 'assets/css/bootstrap-datepicker3.css', false);
        wp_enqueue_style('villa_hotel_load_main', $this->path . 'assets/css/main.css', false);
    }

    public function render_main_page() {
        $form_html = $this->get_form_html();

        $table_html = $this->get_table_html();

        echo $form_html . $table_html;
    }

    private function get_form_html(){
        ob_start();

        include 'templates/form.php';

        return ob_get_clean();
    }

    private function get_table_html(){
        ob_start();

        include 'templates/table.php';

        return ob_get_clean();
    }

    public function load_table(){
        check_ajax_referer( 'villa-hotel-load-nonce', 'nonce_code' );

        echo $this->get_table_html();

        wp_die();
    }

}

add_action( 'init', 'init_villa_hote_load' );
function init_villa_hote_load() {
    VillaHotelLoad::get_instance();
}


add_action( 'wp_ajax_nopriv_villa_update_id_option', 'villa_update_id_option' );
add_action( 'wp_ajax_villa_update_id_option', 'villa_update_id_option' );
function villa_update_id_option() {
    update_option('villa_admin_booking_id' , $_POST['villa_admin_booking_id']);

    wp_die();
}

add_action( 'wp_ajax_nopriv_villa_get_booking_form_by_id', 'villa_get_booking_form_by_id' );
add_action( 'wp_ajax_villa_get_booking_form_by_id', 'villa_get_booking_form_by_id' );
function villa_get_booking_form_by_id() {
    echo cmb2_get_metabox_form('booking_metabox', get_option('villa_admin_booking_id'));
    echo get_option('villa_admin_booking_id');

    wp_die();
}


add_action( 'wp_ajax_nopriv_villa_update_booking', 'villa_update_booking_from_admin' );
add_action( 'wp_ajax_villa_update_booking', 'villa_update_booking_from_admin' );
function villa_update_booking_from_admin() {
    $params = array();
    $booking_id = $_POST['booking_id'];

    $testt = parse_str($_POST['post_data'], $params);
    $cmb = cmb2_get_metabox( 'booking_metabox', $booking_id );
    $cmb->save_fields( $booking_id, $cmb->object_type(), $params );

    wp_die();
}